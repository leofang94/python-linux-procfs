%global _description\
Python classes to extract information from the Linux kernel /proc files.

Name: python-linux-procfs
Version: 0.6.2
Release: 1
Summary: Linux /proc abstraction classes
License: GPLv2
URL: https://rt.wiki.kernel.org/index.php/Tuna
Source: https://git.kernel.org/pub/scm/libs/python/%{name}s/%{name}.git/snapshot/%{name}-%{version}.tar.gz
BuildArch: noarch
BuildRequires: python2-devel python2-setuptools
BuildRequires: python3-devel python3-setuptools

%description %_description

%package -n python2-linux-procfs
Summary: %summary
%{?python_provide:%python_provide python2-linux-procfs}
Requires: python2-six

%description -n python2-linux-procfs %_description

%package -n python3-linux-procfs
Summary: %summary
%{?python_provide:%python_provide python3-linux-procfs}
Requires: python3-six

%description -n python3-linux-procfs %_description

%prep
%autosetup -p1

%build
%py2_build
%py3_build

%install
%py2_install
%py3_install

%files -n python2-linux-procfs
%defattr(0755,root,root,0755)
%{python2_sitelib}/procfs/
%{_bindir}/pflags
%defattr(0644,root,root,0755)
%{python2_sitelib}/*.egg-info
%license COPYING

%files -n python3-linux-procfs
%defattr(0755,root,root,0755)
%{_bindir}/pflags
%{python3_sitelib}/procfs/
%defattr(0644,root,root,0755)
%{python3_sitelib}/*.egg-info
%license COPYING

%changelog
* Fri Jul 24 2020 tianwei <tianwei12@huawei.com> - 0.6.2-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: update to release 0.6.2

* Fri Sep 27 2019 yefei <yefei25@huawei.com> - 0.5.1-8
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: add file permissions

* Mon Sep 16 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.5.1-7
- Package init
